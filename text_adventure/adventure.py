from abc import ABC, abstractmethod


class Event(ABC):
    '''
    Template event that is the base of all types of events in the game
    '''
    @abstractmethod
    def __init__(self):
        pass

    @abstractmethod
    def trigger_event(self):
        pass

    @abstractmethod
    def next_event(self):
        pass


class AdventureEvent(Event):
    '''
    An regular event in an Adventure game, will trigger a next event and ask user input
    '''
    def __init__(self, message: str, possible_events: dict) -> None:
        self.message = message
        self.possible_events = possible_events

    def __repr__(self) -> str:
        return self.message

    def trigger_event(self) -> None:
        print(self)
        while True:
            try:
                # Match case is new to Python 3.10
                # Very poweful, better than a bunch of if else statements
                match input("Enter your input: "):
                    case "n":
                        event = self.possible_events["n"]
                        break
                    case "s":
                        event = self.possible_events["s"]
                        break
                    case "w":
                        event = self.possible_events["w"]
                        break
                    case "e":
                        event = self.possible_events["e"]
                        break
                    case "look":
                        print(self)
                        continue
                    case _:
                        print("Cannot recognise command")
                        continue
            except KeyError:
                print("That way is not possible")
                continue

        self.next_event(event)

    def next_event(self, event: Event) -> None:
        event.trigger_event()


class GameEndEvent(Event):
    '''
    Ends the gasme by calling exit()
    '''
    def __init__(self, message: str):
        self.message = message

    def __repr__(self):
        return self.message

    def trigger_event(self):
        print(self)
        self.next_event()

    def next_event(self):
        exit()
