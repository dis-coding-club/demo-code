from adventure import AdventureEvent, GameEndEvent


def main():

    # The player turns into a crab
    event6 = GameEndEvent(
        message="The machine turns you into a crab and you lived happily ever after as a crustacean"
    )

    event5 = AdventureEvent(
        message="You go into the room and see a button. You instinctively press it and contraption appears magically to the west. It seems that you have to stand in it to activate it.",
        possible_events={"w": event6}
    )

    # the player gets killed by the spider
    spider_end = GameEndEvent(
        message="The light was from a bioluminesance of a massive spider. It eats you and you die a painful death"
    )

    event4 = AdventureEvent(
        message="As you walk towards the mailbox, the ceiling collapsed on top of the entrance, you seem to be stuck in the building. There is a light to the east and a wooden toor to the west",
        possible_events={"e":spider_end, "w": event5}
    )

    game_end1 = GameEndEvent(
        message="You open the trapdoor and hear a rumbling noise. You began to mutter out the words engraved into the dark limestone. The words say Ph'nglui mglw'nafh Cthulhu R'lyeh wgah'nagl. After you finish reading the words, you feel dizzy and collapse and knowledge of forbidden age began pouring into your mind. After an indefinite amount of time, you wake up in a hospiital. On the hospital bed is written your name: Kitab al-Azif."
    )

    event_trapdoor = AdventureEvent(
        message="You go into the building. The hall is so great that you cannot see the end. There are trapdoors on the floor. The latch to open it to the north",
        possible_events={"n": game_end1}
    )

    event_city = AdventureEvent(
        message="You enter the city. There seems to be nobody here, but you still feel uneasy. There is a tall building with a massive gate in the west, you could feel a cold breeze flowing out of the door, which is ajar",
        possible_events={"w": event_trapdoor}
    )

    event_cthultu = AdventureEvent(
        message="You fall into the abyss and land in an oily, black river. You get out and stand on the shore. You see an antedeluvian city looming south of you",
        possible_events={"s": event_city}
    )
    
    event3 = AdventureEvent(
        message="You enter the building. There is a mailbox towards the north. You also see a deep abyss to your west. It looks deep and a foul smell comes out of it",
        possible_events={"n": event4, "w": event_cthultu}
    )

    event2 = AdventureEvent(
        message="You see a building, the entrance is to your west",
        possible_events={"w": event3},
    )
    event1 = AdventureEvent(
        message="You see 4 paths, each north, south, east and west of you",
        possible_events={"n": event2},
    )

    event1.trigger_event()


if __name__ == "__main__":
    main()
