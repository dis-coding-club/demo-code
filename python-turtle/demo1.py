import turtle

t = turtle.Turtle()
window = turtle.Screen()
window.bgcolor("#ffffff")
t.shape("turtle")
t.speed(3)
t.color("green")

d = 0
x = 5
for i in range(100):
    for i in range(50):
        t.right(10)
        t.forward(d)
    d -= x

window.exitonclick()
