from turtle import forward, left, color, begin_fill, end_fill, done, pos, speed

# from tutle import *
# This is bad style because it pollutes the namespace with lots of methods
# https://docs.python.org/3/library/turtle.html
color("#b7c62b", "#43994e")  # Sets color to red and yellow
begin_fill()  # called before shape is to be filled
speed(2)
while True:
    forward(331)  # goes 200 pixels to the direction the turtle is facing
    left(99)  # turns left 170 degrees
    if (
        abs(pos()) < 1
    ):  # gets the position of turtle, it can be on negative x axis, breaks out of loop once the turtle does a full cycle
        break
end_fill()  # stops the cursor from filling
done()  # ends drawing process
