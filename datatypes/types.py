# Booleans
x = True
y = False

# None
x = None
print(type(x))

# Ints and floats
x = 5
print(x)
y = 1.5
print(int(y))

# Tuples
foo = (1, 2.6, 3, "hello")
print(foo)
print(foo[-1])
print(foo[0])

# Lists
bar = [1, 2, 3, "hello", ["first element", "2nd element"]]
print(bar)
print(bar[-1][0])
bar.append("world")
print(bar)
bar[3] = "this is the 3rd element"
print(bar)
bar.pop(-1)
print(bar)

# Dictionaries
dictionary = {
    "foo": 12,
    "bar": "qwerty",
    "fizz": (0.5, 1.5, "hello"),
    "buzz": {"key1": "value1", "key2": "value2"},
}
print(dictionary["foo"])
print(dictionary["buzz"]["key1"])
dictionary["bar"] = "vim is the best"
print(dictionary["bar"])
dictionary.pop("bar")
dictionary["fuzz"] = "new element"
print(dictionary)

# sets
example_set = {42, "values in set must be unique"}
example_set.pop() # sets dont have index
print(example_set)
example_set.add("new element")

