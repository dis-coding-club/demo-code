from flask import Flask, render_template, jsonify
import datetime

app = Flask(__name__)

@app.route("/time", methods=["GET"])
def curr_time():
    return jsonify({"time": datetime.datetime.now()})

@app.route("/add_one/<number>", methods=["POST"])
def add_one(number):
    try:
        new_number = int(number) + 1
        return jsonify({
            "orig_number": int(number)
            "new_number": new_number
        })
    except ValueError :
        return jsonify({
            "err": "Cannot convert input to an integer"
        }), 400

@app.route("/")
def index():
    return render_template("index.html")


if __name__ == "__main__":
    app.run()
