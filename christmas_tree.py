# Python Program to Generate Christmas Tree Pattern

# Generating Triangle Shape
def triangle_shape(n):
    for i in range(n):
        for _ in range(n - i):
            print(" ", end=" ")
        for _ in range(2 * i + 1):
            print("*", end=" ")
        print()


# Generating Pole Shape
def pole_shape(n):
    for _ in range(n):
        for _ in range(n - 1):
            print(" ", end=" ")
        print("  * ")


# Input and Function Call
row = int(input("Enter number of rows: "))

triangle_shape(row)
triangle_shape(row)
pole_shape(row)
