class Person:
    def __init__(self, name: str, age: int): # python is weakly typed
        self.name = name
        self.age = age

    def say_hello(self):
        print(f"Hello, I am {self.name}")

#
person1 = Person("John", 20)
# person1.say_hello()
# print(person1.name)
# print(type(person1))
#
class Employee(Person):
    def __init__(self, name: str, age: int, company: str, salary: int | float):
        super().__init__(name, age) # super refers to father class
        self.company = company
        self._salary = salary # pseudo hiding of values, convention with underscore

    @property # makes the method act like properties
    def get_salary(self):
        return self._salary

    def change_company(self, new_company):
        self.company = new_company


employee1 = Employee("Smith", 22, "Meta", 420000)
# employee1.say_hello()
# print(employee1.get_salary)
# employee1.change_company("Amazon")
# print(employee1.company)
print(isinstance(employee1, Employee))
print(isinstance(employee1, Person))
print(isinstance(person1, Employee))
