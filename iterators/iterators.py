iterable = [1, 2, 3, 4, 5]
# create an iterator object from that iterable
iter_obj = iter(iterable)
# iter_obj = iterable.__iter__()

# infinite loop
while True:
    try:
        # get the next item
        element = next(iter_obj)
        print(element)
        # do something with element
    except StopIteration:
        # if StopIteration is raised, break from loop
        break

# The while loop is the same as this
for element in iterable:
    print(element)
