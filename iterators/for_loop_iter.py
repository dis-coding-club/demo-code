# example_list = [1, 2, 3, 4, "five", "hello", 3.14]
#
# for i in example_list:
#     print(i)

# example_tuple = ("hello", "darkness", "my", "old", "friend")
#
# for i in example_tuple:
#     print(i)
#     # for chars in i:
    #     print(chars)

# for i in range(10):
    # print(f"Hello {i}")

# d = {"name": "Alice", "age": 23, "country": "NL"}
# for i in d:
    # print(i)

# for i in d.values():
    # print(i)
#
# mystring = "hello"
# for i in mystring:
#     print(i)
#
