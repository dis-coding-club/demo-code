import random


def main():
    while True:
        random_number = random.randint(1, 10)
        while True:
            guess = input("Guess a number: ")
            try:
                guess = int(guess)
            except ValueError:
                print("Enter a valid integer")
                continue
            if guess == random_number:
                print(
                    f"The guess is correct, your guess is {guess} and the answer is {random_number}"
                )
                break
            else:
                print("Try again")
                continue


if __name__ == "__main__":
    main()
