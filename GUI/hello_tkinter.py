import tkinter as tk
from tkinter import ttk

window = tk.Tk()
frame = ttk.Frame(window, padding=10)
frame.grid()

window.title("Demo")

greeting = ttk.Label(
    master=frame, text="Hello, Tkinter", foreground="blue", background="white"
).grid(column=0, row=0)

button = ttk.Button(
    master=frame,
    text="Close Window",
    command=window.destroy,  # Function to terminate the parent window
).grid(column=0, row=1)

window.mainloop()  # Tells window to open
