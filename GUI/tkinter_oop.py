from tkinter import BOTH, Frame, Button, Tk, Label


class Window(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        Frame.grid(self)
        self.master = master
        self.title = "Demo2"

        # widget can take all window
        self.pack(fill=BOTH, expand=1)

        # create button, link it to clickExitButton()
        exitButton = Button(self, text="Exit", command=self.clickExitButton)

        # place button at (0,0)
        exitButton.grid(column=0, row=1)

        demo_label = Label(self, text="Hello World!")

        demo_label.grid(column=0, row=0)

    def clickExitButton(self):
        exit()


root = Tk()
app = Window(root)
root.wm_title("Tkinter button")
root.geometry("320x200")
root.mainloop()
