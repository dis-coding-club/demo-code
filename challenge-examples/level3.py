from dataclasses import dataclass

# Overengineered, perhaps
@dataclass(slots=True)
class Coordinate:
    x: float
    y: float

    def calc_gradient(self, coord2):
        return (coord2.x - self.x) / (coord2.y - self.y)


class CoordinateFactory:
    def create_from_input(self) -> Coordinate:
        try:
            x = float(input("Enter x coord: "))
            y = float(input("Enter y coord: "))
            return Coordinate(x, y)
        except ValueError:
            print(f"Please enter a valid number! Defaulting to 0,0")
            return Coordinate(0, 0)


factory = CoordinateFactory()
coord1 = factory.create_from_input()
coord2 = factory.create_from_input()

print(coord1.calc_gradient(coord2))
