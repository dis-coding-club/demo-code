def main():
    values = list()
    for i in range(2):
        message_x = f"Enter coordinate for x{i+1}"
        values.append(int(input(message_x)))
    for i in range(2):
        message_x = f"Enter coordinate for y{i+1}"
        values.append(int(input(message_x)))
    print(
        "the gradient of the slope is: {0}".format(
            (values[0] - values[1]) / (values[2] - values[3])
        )
    )


if __name__ == "__main__":
    main()
