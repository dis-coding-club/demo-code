use std::io::{stdin, stdout, Write};
use std::error::Error;

#[derive(Debug)]
struct Coordinate {
    x: f32,
    y: f32
}

impl Coordinate {
    fn new() -> Result<Self, Box<dyn Error>> {
        let mut user_input = String::new();
        let handle = stdin();
        stdout().flush()?;
        println!("Enter the x y coordinates separated by commas");
        handle.read_line(&mut user_input)?;
        for _ in 1..2 {
            user_input.pop();
        }
        let coords: Vec<f32> =  user_input.split(',')
            .collect::<Vec<&str>>()
            .iter()
            .filter_map(|s| s.parse::<f32>().ok())
            .collect::<Vec<f32>>();
        println!("{:?}", coords);
        return Ok(
            Coordinate { x: coords.first().unwrap().to_owned(), 
                y: coords.last().unwrap().to_owned() }
        )
    }

    fn calc_gradient(&self, coord2: &Coordinate) -> f32 {
        (self.x-coord2.x)/(self.y-coord2.y)
    }
}

fn main() {
    // let coord1 = Coordinate{
    //     x: 16.43,
    //     y: 123.21
    // };
    // let coord2 = Coordinate{
    //     x: 90.458,
    //     y: 29.218
    // };
    // println!("{}",coord1.calc_gradient(coord2));

    let coord1 = Coordinate::new().expect("INVALID_INPUT");
    let coord2 = Coordinate::new().expect("INVALID_INPUT");
    println!("Here are the coordinates: {:?}, {:?}", coord1, coord2);
    println!("The gradient is: {}",coord1.calc_gradient(&coord2));
}
