# Function to draw a tree with a given height and level
def draw_tree(height, level):
  # Check if the level is equal to the height of the tree
  if level == height:
    # Return if the level is equal to the height
    return
  # Print the spaces before the asterisks on each row
  for j in range(height - level):
    print(" ", end="")
  # Print the asterisks on each row
  for j in range(2 * level - 1):
    print("*", end="")
  # Move to the next line
  print()
  # Call the function recursively with the next level
  draw_tree(height, level + 1)
# Call the function to draw a tree with a height of 5
draw_tree(5, 1)
