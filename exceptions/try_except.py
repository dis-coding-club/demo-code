# We did not define x
try:
    print(x)
except NameError:
    print("Variable x is not defined")

try:
    int("This is a string")
    # If error, it goes directly to the except block
    print("Executed")
except ValueError:
    print("Try using an integer")
finally:
    print("No matter what this will execute")


def square_a_number(number):
    if type(number) != int and type(number) != float:
        raise ValueError("Use a number")
    else:
        return number**2


print(square_a_number(2.1238))
try:
    print(square_a_number("helloworld"))
except ValueError as e:
    print(e)


while True:
    try:
        print("hello")
    except:
        continue
